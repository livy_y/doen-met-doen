#!/bin/bash

# Check if the user is on Windows or Linux
if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    VENV_NAME="venv"
    VENV_ACTIVATE="./${VENV_NAME}/bin/activate"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    VENV_NAME="venv"
    VENV_ACTIVATE="./${VENV_NAME}/Scripts/activate"
    # Check if virtualenv is installed
    if ! command -v virtualenv &> /dev/null; then
        echo "virtualenv is not installed. Installing..."
        python -m pip install virtualenv
    fi
else
    echo "Unsupported operating system"
    exit 1
fi

# Create virtual environment
python -m venv $VENV_NAME

# Activate virtual environment
source $VENV_ACTIVATE

# Install requirements
pip install -r requirements.txt

echo "Virtual environment created, activated, and requirements installed."
