# Constants and Variables
WIDTH, HEIGHT = 1700, 1000  # Set window dimensions
FPS = 60  # Set frames per second for the game
FONT = "assets/font/pixel-font.ttf"
FONT_COLOR = (247, 150, 23)
BACKGROUND_COLOR = (69, 41, 63)
SPEED = 4