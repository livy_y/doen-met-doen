import sys, os

# Add the parent directory to sys.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import json
import threading
from pathlib import Path
from backend import UART

from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QLabel, QLineEdit, QPushButton, \
    QMessageBox, QSplitter, QTextEdit, QSpacerItem, QSizePolicy

from menu.plot import show_plots

class SettingsEditor(QWidget):
    def __init__(self, json_file):
        super().__init__()
        self.json_file = json_file
        self.settings = self.load_settings()

        self.init_ui()

        self.timer = QTimer()
        self.timer.timeout.connect(self.update_ui)
        self.timer.start(16)  # Update approximately every 16 milliseconds (about 60 frames per second)

        self.uart = UART.SerialPortListener()

    def init_ui(self):
        self.setStyleSheet(Path('assets/theme.qss').read_text())
        layout = QVBoxLayout()

        section_list = self.create_sections()
        splitter = self.create_splitter(section_list)
        layout.addWidget(splitter)

        hardware_buttons = self.create_hardware_buttons()
        layout.addLayout(hardware_buttons)

        self.console_widget = self.create_console()
        layout.addWidget(self.console_widget)

        self.setLayout(layout)

        self.setWindowTitle("Settings Editor")
        self.setMinimumSize(700, 500)  # Set minimum window size

    def update_ui(self):
        uart_message = self.uart.get_raw_messages()
        if uart_message:
            self.append_text_to_console(uart_message)

    def upload_settings(self):
        result = self.uart.upload_file("assets/settings.json", "settings.json")
        if result:
            self.append_text_to_console(result)

    def connect_hardware(self):
        result = self.uart.find_serial_port()
        if result:
            self.append_text_to_console(result)

    def show_plots(self):
        # Create a thread and start it
        thread = threading.Thread(target=show_plots)
        thread.start()

    def create_hardware_buttons(self):
        #self.button_upload = QPushButton("Upload Settings")
        #self.button_upload.setObjectName("hardware_button")
        #self.button_upload.clicked.connect(self.upload_settings)

        self.button_plot = QPushButton("Plots")
        self.button_plot.setObjectName("hardware_button")
        self.button_plot.clicked.connect(self.show_plots)

        self.button_connect = QPushButton("Connect to Hardware")
        self.button_connect.setObjectName("hardware_button")
        self.button_connect.clicked.connect(self.connect_hardware)

        # Align buttons to the right using QHBoxLayout
        layout = QHBoxLayout()
        layout.addStretch(1)  # Add stretchable space to push buttons to the right
        layout.addWidget(self.button_connect)
        layout.addWidget(self.button_plot)
        #layout.addWidget(self.button_upload)

        return layout

    def create_console(self):
        console_widget = QTextEdit()
        console_widget.setObjectName("console")
        console_widget.setReadOnly(True)
        return console_widget

    def append_text_to_console(self, text):
        self.console_widget.append(text)

    def create_sections(self):
        section_list = []
        for setting, options in self.settings.items():
            section = self.create_section(setting, options)
            section_list.append(section)
        return section_list

    def create_section(self, setting, options):
        section = QVBoxLayout()
        section.setAlignment(Qt.AlignTop)

        setting_label = QLabel(setting)
        setting_label.setObjectName("heading")
        section.addWidget(setting_label)

        h_layout = QVBoxLayout()
        for key, value in options.items():
            if isinstance(value, bool):
                checkbox = self.create_checkbox(value, key, setting)
                h_layout.addWidget(checkbox)
            else:
                label, input_field = self.create_input_field(value, key, setting)
                h_layout.addWidget(label)
                h_layout.addWidget(input_field)

        section.addLayout(h_layout)
        return section

    def create_checkbox(self, value, key, setting):
        checkbox = QPushButton("Enabled" if value else "Disabled")
        checkbox.clicked.connect(
            lambda state, btn=checkbox, k=key, s=setting: self.toggle_checkbox(btn, s, k))
        return checkbox

    def create_input_field(self, value, key, setting):
        label = QLabel(f"{key}:")
        input_field = QLineEdit(str(value))
        input_field.setFixedWidth(100)
        input_field.textChanged.connect(
            lambda text, k=key, s=setting: self.update_setting(text, s, k))
        return label, input_field

    def create_splitter(self, section_list):
        splitter = QSplitter(Qt.Horizontal)
        left = QVBoxLayout()
        right = QVBoxLayout()
        for index, section in enumerate(section_list):
            if index < len(section_list) / 2:
                left.addLayout(section)
                spacer_item = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
                left.addItem(spacer_item)
            else:
                right.addLayout(section)
                spacer_item = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
                left.addItem(spacer_item)
        splitter.addWidget(QWidget())
        splitter.addWidget(QWidget())
        splitter.widget(0).setLayout(left)
        splitter.widget(1).setLayout(right)
        return splitter

    def load_settings(self):
        try:
            with open(self.json_file, 'r') as file:
                settings = json.load(file)
                return settings
        except FileNotFoundError:
            QMessageBox.critical(self, "Error", "JSON file not found.")
            sys.exit(1)
        except json.JSONDecodeError:
            QMessageBox.critical(self, "Error", "Invalid JSON format.")
            sys.exit(1)

    def update_setting(self, value, setting, key):
        try:
            self.settings[setting][key] = int(value)
            self.save_settings()
        except ValueError:
            QMessageBox.critical(self, "Error", "Invalid value. Please enter an integer.")

    def toggle_checkbox(self, btn, setting, key):
        self.settings[setting][key] = not self.settings[setting][key]
        btn.setText("Enabled" if self.settings[setting][key] else "Disabled")
        self.save_settings()

    def save_settings(self):
        with open(self.json_file, 'w') as file:
            json.dump(self.settings, file, indent=4)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    editor = SettingsEditor("assets/settings.json")
    editor.show()
    sys.exit(app.exec_())
