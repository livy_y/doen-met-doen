from rshell.pyboard import Pyboard
from ampy import files
import serial.tools.list_ports
import serial
import json
from time import sleep

def put_file_to_esp32(port, local_file, remote_path):
    """
    Copy a file from the local system to a specified path on the ESP32 device.

    Args:
    - board: Pyboard instance representing the ESP32 board.
    - local_file: Path to the local file to be transferred.
    - remote_path: Destination path on the ESP32 device.
    """
    board = Pyboard(port)

    with open(local_file, "rb") as infile:
        data = infile.read()
        board_files = files.Files(board)
        board_files.put(remote_path, data)

class SerialPortListener:
    def __init__(self):
        self.serial_port = None
        self.current_port = ""

    def find_serial_port(self):
        """
        Find an available serial port automatically.
        """
        ports = serial.tools.list_ports.comports()
        for port, desc, hwid in sorted(ports):
            if port !="/dev/ttyS0":
                try:
                    self.serial_port = serial.Serial(port, 115200)
                    self.current_port = port
                    print("Conneted to: " + port)
                    return "The device is connected."
                except serial.SerialException:
                    continue
        return "The device is not connected."

    def pause_serial_port(self):
        """
        Pauses the serial port connection.
        """
        if self.serial_port:
            self.serial_port.close()
            print("Serial port communication paused.")
        else:
            print("No serial port connection available.")

    def resume_serial_port(self):
        """
        Resumes the serial port connection.
        """
        if self.serial_port:
            self.serial_port.open()
            print("Serial port communication resumed.")
        else:
            print("No serial port connection available.")

    def upload_file(self, file_path, destination_path):
        """
        Put a file onto the ESP32 device through serial port.
        """
        if self.serial_port is None or not self.serial_port.is_open:
            print("Serial port not open or not connected.")
            return

        self.pause_serial_port()

        try:
            put_file_to_esp32(self.current_port, file_path, destination_path)

            print(f"File '{file_path}' sent successfully to '{destination_path}' on ESP32.")
        except FileNotFoundError:
            print(f"File '{file_path}' not found.")

        self.resume_serial_port()

    def extract_json(self, line):
        """
        Extracts JSON data from the input line.

        Args:
            line (str): The input line containing JSON data.

        Returns:
            dict: The extracted JSON data, or None if no valid JSON data is found.
        """
        if line is None:
            return None

        # Check if the line starts with "start_commander<" and ends with ">end"
        if line.startswith("start_commander<") and line.endswith(">end"):
            # Extract JSON data between "start_commander<" and ">end"
            data_str = line[len("start_commander<"):-len(">end")]
            try:
                data = json.loads(data_str)
                print(data)
                return data
            except json.JSONDecodeError:
                print("Error: Failed to parse JSON: " + line)
                return None
        # Check if the line starts with "start_bracelet<" and ends with ">end"
        elif line.startswith("start_bracelet<") and line.endswith(">end"):
            # Extract JSON data between "start_bracelet<" and ">end"
            data_str = line[len("start_bracelet<"):-len(">end")]
            try:
                data = json.loads(data_str)
                print(data)
                return data
            except json.JSONDecodeError:
                print("Error: Failed to parse JSON: " + line)
                return None
        else:
            print("Error: Line format is invalid")
            return None


    def get_raw_messages(self):
        if not self.serial_port:
            return

        if self.serial_port.in_waiting > 0:
            message = self.serial_port.readline().decode().strip()
            return message

    def is_enabled(self):
        if self.serial_port:
            return True
        else:
            print("Serial connection is not available!")
            return False

    def get_processed_messages(self):
        raw = self.get_raw_messages()
        return self.extract_json(raw)


    def start_listening(self):
        """
        Start listening for messages on the serial port.
        """
        if not self.find_serial_port():
            print("Error: No serial port available.")
            return

        print("Listening for messages ...")
        while True:
            if self.serial_port.in_waiting > 0:
                message = self.serial_port.readline().decode().strip()
                self.process_message(message)

if __name__ == "__main__":
    listener = SerialPortListener()
    listener.start_listening()