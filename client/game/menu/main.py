import json
from datetime import datetime

import pygame, constants
import sys, subprocess
from game.hud import Text

class Button:
    def __init__(self, text, position, size, font_size=64):
        self.text = text
        self.position = position
        self.size = size
        self.rect = pygame.Rect(position, size)
        self.font = pygame.font.Font(constants.FONT, font_size)
        self.hovered = False

    def draw(self, surface):
        color = (69, 41, 63)
        if self.hovered:
            color = (51, 27, 45)
        pygame.draw.rect(surface, color, self.rect)
        text_surface = self.font.render(self.text, True, constants.FONT_COLOR)
        text_rect = text_surface.get_rect(center=self.rect.center)
        surface.blit(text_surface, text_rect)


class MainMenu:
    def __init__(self, screen):
        self.screen = screen
        self.background = pygame.image.load('assets/menu_background.png')
        self.clock = pygame.time.Clock()
        self.buttons = []
        self.create_buttons()

        self.text = Text(self.last_exercises(), color=(0, 0, 0))
        self.text.rect.topleft = (10, 10)

    def create_buttons(self):
        button_width = 600
        button_height = 100
        button_x = constants.WIDTH * (3 / 5)
        button_y = constants.HEIGHT * (4 / 5)

        play_button = Button("Play",
                             ((constants.WIDTH - button_width) / 2, (constants.HEIGHT - button_height) / 2),
                             (button_width * (4 / 5), button_height * (4 / 3)),
                             100)

        settings_button = Button("Settings", (button_x, button_y-70), (button_width, button_height))
        quit_button = Button("Quit", (button_x, button_y + 70), (button_width, button_height))
        self.buttons.extend([play_button, settings_button, quit_button])


    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    for button in self.buttons:
                        if button.rect.collidepoint(event.pos):
                            if button.text == "Play":
                                return "game"
                            elif button.text == "Quit":
                                pygame.quit()
                                sys.exit()
                            elif button.text == "Settings":
                                subprocess.Popen(["python", "settings/main.py"], cwd="./")

    def last_exercises(self):
        # Load data from JSON file
        with open("assets/exercise_data.json", 'r') as file:
            data = json.load(file)

        # Extract latest date and exercises
        latest_entry = data[-1]
        latest_date = latest_entry['date']
        latest_exercises = latest_entry['exercises']

        # Convert date string to datetime object
        date_obj = datetime.strptime(latest_date, "%Y-%m-%d")

        # Format the date string
        formatted_date = date_obj.strftime("%d %B")

        # Create the desired string
        result_string = f"Op {formatted_date} heb je {latest_exercises} oefeningen gedaan."
        return result_string

    def run(self):
        while True:
            self.screen.blit(self.background, (0, 0))
            self.text.draw(self.screen)
            choice = self.handle_events()

            if choice:
                return choice

            for button in self.buttons:
                if button.rect.collidepoint(pygame.mouse.get_pos()):
                    button.hovered = True
                else:
                    button.hovered = False
                button.draw(self.screen)
            pygame.display.flip()
            self.clock.tick(30)
