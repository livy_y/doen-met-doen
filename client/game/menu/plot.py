import json

import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

sns.set_style("whitegrid")
blue, = sns.color_palette("muted", 1)

class MultiPlot:
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.fig, self.plots = plt.subplots(rows, cols)
        self.fig.tight_layout()
        self.current_row = 0
        self.current_col = 0

    def add_plot(self, func, title=''):
        ax = self.plots[self.current_row, self.current_col]
        x = np.arange(1, 10)
        func(ax, x)
        ax.set_title(title, fontweight="bold")

        # Move to the next plot position
        self.current_col += 1
        if self.current_col == self.cols:
            self.current_col = 0
            self.current_row += 1
            if self.current_row == self.rows:
                self.current_row = 0

# Example functions for plots
def plot_square_root(ax, x):
    ax.plot(x, x**0.5)

def plot_exponent(ax, x):
    ax.plot(x, np.exp(x))

def plot_square(ax, x):
    ax.plot(x, x*x)


def plot_exercise_data(ax, x):
    with open("assets/exercise_data.json", 'r') as f:
        data = json.load(f)

    dates = [entry['date'][5:] for entry in data]
    exercises = [entry['exercises'] for entry in data]

    ax.plot(dates, exercises, color=blue, lw=3)
    ax.fill_between(dates, exercises, alpha=.3)

    ax.set_xlabel('Datum')
    ax.set_ylabel('Exercises')
    ax.tick_params(axis='x', rotation=45)

def plot_jump_count(ax, x):
    with open("assets/jump_data.json", 'r') as f:
        data = json.load(f)

    dates = [entry['date'] for entry in data]

    daily_jump_count = {}

    for date in dates:
        day = date[5:-9]

        try:
            daily_jump_count[day] += 1
        except KeyError:
            daily_jump_count[day] = 1

    ax.plot(daily_jump_count.keys(), daily_jump_count.values(), color=blue, lw=3)
    ax.fill_between(daily_jump_count.keys(), daily_jump_count.values(), alpha=.3)

    ax.set_xlabel('Dag')
    ax.set_ylabel('Aantal sprongen')
    ax.tick_params(axis='x', rotation=45)

def plot_jump_height(ax, x):
    with open("assets/jump_data.json", 'r') as f:
        data = json.load(f)

    dates = [entry['date'] for entry in data]
    heights = [entry['jump_height']*100 for entry in data]

    daily_jump_heights = {}

    for index, date in enumerate(dates):
        day = date[5:-9]

        try:
            daily_jump_heights[day].append(heights[index])
        except KeyError:
            daily_jump_heights[day] = [heights[index]]

    daily_jump_height = {}

    for key, value in daily_jump_heights.items():
        daily_jump_height[key] = filtered_average(value, 50)

    ax.plot(daily_jump_height.keys(), daily_jump_height.values(), color=blue, lw=3)
    ax.fill_between(daily_jump_height.keys(), daily_jump_height.values(), alpha=.3)

    ax.set_xlabel('Dag')
    ax.set_ylabel('Sprong hoogte')
    ax.tick_params(axis='x', rotation=45)

def filtered_average(numbers, threshold):
    # Filter out values greater than the threshold
    filtered_numbers = [x for x in numbers if x <= threshold]

    # Calculate the average
    if filtered_numbers:
        average = sum(filtered_numbers) / len(filtered_numbers)
        return average
    else:
        return None

def plot_squad_count(ax, x):
    with open("assets/squad_data.json", 'r') as f:
        data = json.load(f)

    dates = [entry['date'] for entry in data]

    daily_squad_count = {}

    for date in dates:
        day = date[5:-9]

        try:
            daily_squad_count[day] += 1
        except KeyError:
            daily_squad_count[day] = 1

    ax.plot(daily_squad_count.keys(), daily_squad_count.values(), color=blue, lw=3)
    ax.fill_between(daily_squad_count.keys(), daily_squad_count.values(), alpha=.3)

    ax.set_xlabel('Dag')
    ax.set_ylabel('Aantal squads')
    ax.tick_params(axis='x', rotation=45)

def plot_squad_threshold_reached(ax, x):
    with open("assets/squad_data.json", 'r') as f:
        data = json.load(f)

    dates = [entry['date'] for entry in data]
    thresholds_reached = [entry['threshold_reached'] for entry in data]

    daily_thresholds_reached = {}

    for index, date in enumerate(dates):
        day = date[5:-9]

        try:
            daily_thresholds_reached[day].append(thresholds_reached[index])
        except KeyError:
            daily_thresholds_reached[day] = [thresholds_reached[index]]

    # Calculate percentage achieved and not achieved for each day
    result_data = {}
    for day, entries in daily_thresholds_reached.items():
        percentage_achieved = (entries.count(True) / len(entries)) * 100
        result_data[day] = percentage_achieved

    percentage_not_achieved = [100 - value for value in result_data.values()]

    ax.bar(result_data.keys(), result_data.values(), color="green", label="Ja")
    ax.bar(result_data.keys(), percentage_not_achieved, color="red", label="Ja")

    ax.set_xlabel('Dag')
    ax.tick_params(axis='x', rotation=45)

def plot_squad_speed(ax, x):
    with open("assets/squad_data.json", 'r') as f:
        data = json.load(f)

    dates = [entry['date'] for entry in data]
    squads = [entry['average_speed'] for entry in data]

    daily_squad_speeds = {}

    for index, date in enumerate(dates):
        day = date[5:-9]

        try:
            daily_squad_speeds[day].append(squads[index])
        except KeyError:
            daily_squad_speeds[day] = [squads[index]]

    daily_squad_speed = {}

    for key, value in daily_squad_speeds.items():
        daily_squad_speed[key] = filtered_average(value, 10)

    ax.plot(daily_squad_speed.keys(), daily_squad_speed.values(), color=blue, lw=3)
    ax.fill_between(daily_squad_speed.keys(), daily_squad_speed.values(), alpha=.3)

    ax.set_xlabel('Dag')
    ax.set_ylabel('Gemiddelde squad snelheid')
    ax.tick_params(axis='x', rotation=45)

def plot_squad_duration(ax, x):
    with open("assets/squad_data.json", 'r') as f:
        data = json.load(f)

    dates = [entry['date'] for entry in data]
    squads = [entry['duration']/1000 for entry in data]

    daily_squad_durations = {}

    for index, date in enumerate(dates):
        day = date[5:-9]

        try:
            daily_squad_durations[day].append(squads[index])
        except KeyError:
            daily_squad_durations[day] = [squads[index]]

    daily_squad_duration = {}

    for key, value in daily_squad_durations.items():
        daily_squad_duration[key] = filtered_average(value, 10)

    ax.plot(daily_squad_duration.keys(), daily_squad_duration.values(), color=blue, lw=3)
    ax.fill_between(daily_squad_duration.keys(), daily_squad_duration.values(), alpha=.3)

    ax.set_xlabel('Dag')
    ax.set_ylabel('Squad tijd in seconden')
    ax.tick_params(axis='x', rotation=45)

def plot_reaction_speed_count(ax, x):
    with open("assets/reaction_speed_data.json", 'r') as f:
        data = json.load(f)

    dates = [entry['date'] for entry in data]

    daily_reaction_speed_count = {}

    for date in dates:
        day = date[5:-9]

        try:
            daily_reaction_speed_count[day] += 1
        except KeyError:
            daily_reaction_speed_count[day] = 1

    ax.plot(daily_reaction_speed_count.keys(), daily_reaction_speed_count.values(), color=blue, lw=3)
    ax.fill_between(daily_reaction_speed_count.keys(), daily_reaction_speed_count.values(), alpha=.3)

    ax.set_xlabel('Dag')
    ax.set_ylabel('Aantal reactie snelheid oefeningen')
    ax.tick_params(axis='x', rotation=45)

def plot_reaction_duration(ax, x):
    with open("assets/reaction_speed_data.json", 'r') as f:
        data = json.load(f)

    dates = [entry['date'] for entry in data]
    durations = [entry['duration'] for entry in data]

    daily_jump_durations = {}

    for index, date in enumerate(dates):
        day = date[5:-9]

        try:
            daily_jump_durations[day].append(durations[index])
        except KeyError:
            daily_jump_durations[day] = [durations[index]]

    daily_reaction_duration = {}

    for key, value in daily_jump_durations.items():
        daily_reaction_duration[key] = filtered_average(value, 100)

    ax.plot(daily_reaction_duration.keys(), daily_reaction_duration.values(), color=blue, lw=3)
    ax.fill_between(daily_reaction_duration.keys(), daily_reaction_duration.values(), alpha=.3)

    ax.set_xlabel('Dag')
    ax.set_ylabel('Reactie snelheid')
    ax.tick_params(axis='x', rotation=45)

def show_plots():
    # Creating an instance of MultiPlot
    multi_plot = MultiPlot(3, 3)

    # Adding plots
    multi_plot.add_plot(plot_exercise_data, 'Aantal oefeningen')
    multi_plot.add_plot(plot_jump_count, "Aantal sprongen")
    multi_plot.add_plot(plot_squad_count, "Aantal squads")
    multi_plot.add_plot(plot_reaction_speed_count, "Aantal reactie snelheid oefeningen")
    multi_plot.add_plot(plot_jump_height, "Sprong hoogte")
    multi_plot.add_plot(plot_reaction_duration, "Reactie snelheid")
    multi_plot.add_plot(plot_squad_threshold_reached, "Diep genoeg gesquad")
    multi_plot.add_plot(plot_squad_speed, "Gemiddelde squad snelheid")
    multi_plot.add_plot(plot_squad_duration, "Squad tijd")

    # Showing all plots
    plt.show()
