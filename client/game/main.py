import sys

from menu.main import MainMenu
from game.game import Game
from game.done import Done
def main():
    game = Game()
    menu = MainMenu(game.display_surface)
    done = Done(game.display_surface)

    current_screen = "menu"

    while True:
        if current_screen == "menu":
            choice = menu.run()
            if choice == "game":
                current_screen = "game"
        elif current_screen == "game":
            game.run()
            current_screen = "done"
        elif current_screen == "done":
            done.run()
            sys.exit()


if __name__ == "__main__":
    main()
