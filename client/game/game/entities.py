import json
from datetime import datetime

import pygame
import constants
from random import random

from game import hud


class EnemySpawner():
    def __init__(self, squat_chance, jump_height_chance, reaction_speed_chance):
        self.enemies = []
        self.min_spawn_interval = 200
        self.spawn_timer = self.min_spawn_interval
        self.hit_enemy = 100
        self.random_identifier = 0

        self.squat_chance = squat_chance
        self.jump_height_chance = jump_height_chance
        self.reaction_speed_chance = reaction_speed_chance

    def spawn(self):
        # Check if enough time has passed to spawn a new enemy
        if self.spawn_timer >= self.min_spawn_interval:
            # Spawn a new enemy with a random chance
            seed = random()*100
            if seed < self.jump_height_chance:  # Adjust the probability as needed
                new_enemy = Enemy()
                self.append_to_json("jump")
                self.enemies.append(new_enemy)
                self.despawn()
            elif seed < self.squat_chance + self.jump_height_chance:
                new_bird = Bird()
                self.append_to_json("squat")
                self.enemies.append(new_bird)
                self.despawn()
            elif seed < self.squat_chance + self.jump_height_chance + self.reaction_speed_chance:
                new_present = Present()
                self.append_to_json("reactie snelheid")
                self.enemies.append(new_present)
                self.despawn()

            # Reset the spawn timer with a random interval
            self.spawn_timer = 0

        # Update the spawn timer
        self.spawn_timer += 1

    def append_to_json(self, exercise):
        # Define exercise types
        valid_exercises = ["jump", "squat", "reactie snelheid"]

        # Check if provided exercise is valid
        if exercise not in valid_exercises:
            print("Invalid exercise type.")
            return

        # Load existing data from JSON file if it exists
        try:
            with open('assets/specific_exercises.json', 'r') as file:
                data = json.load(file)
        except FileNotFoundError:
            data = []

        # Get current date
        current_date = datetime.now().strftime("%Y-%m-%d")

        # Check if data is empty or if the last entry is not from today
        if not data or data[-1]['date'] != current_date:
            # Add a new entry for today's date
            new_entry = {'date': current_date}
            for ex in valid_exercises:
                new_entry[ex] = 0  # Initialize all exercises to 0
            data.append(new_entry)

        # Increment the count for the provided exercise for the last entry
        for entry in data:
            if entry['date'] == current_date:
                entry[exercise] += 1

        # Write data back to JSON file
        with open('assets/specific_exercises.json', 'w') as file:
            json.dump(data, file, indent=4)

    def despawn(self):
        for index, enemy in enumerate(self.enemies):
            if enemy.rect.bottomright[0] < 0:

                self.enemies.pop(index)

                if index < self.hit_enemy != 100:
                    self.hit_enemy -= 1
                if index == self.hit_enemy:
                    self.hit_enemy = 100

    def check_collision(self, rect):
        for index, enemy in enumerate(self.enemies):
            if rect.colliderect(enemy.rect) and not self.hit_enemy == index and not enemy.rand == self.random_identifier:
                self.hit_enemy = index
                self.random_identifier = enemy.rand
                return enemy.exercise_type

    def move(self):
        for enemy in self.enemies:
            enemy.move()

    def draw(self, display_surface):
        for enemy in self.enemies:
            enemy.draw(display_surface)


class Enemy(pygame.sprite.Sprite):
    """Enemy class represents the opponent(cactus,bird,wall) in the game."""

    def __init__(self):
        """Initialize the enemy object."""
        super().__init__()

        # Load image
        self.image = pygame.image.load("assets/monster.png")
        self.image_width = 64
        self.image_height = 64

        self.exercise_type = "jump"
        self.rand = random()

        # Scale image
        self.image_scale_factor = 4
        self.image = pygame.transform.scale(self.image, (self.image_width * self.image_scale_factor,
                                                              self.image_height * self.image_scale_factor))

        self.rect = self.image.get_rect()
        self.rect.bottomleft = (constants.WIDTH, constants.HEIGHT * 3 / 4)

    def move(self):
        """Move the enemy object."""
        self.rect.move_ip(-constants.SPEED, 0)

    def draw(self, surface):
        """Draw the enemy object on the surface."""
        surface.blit(self.image, self.rect)

class Bird():
    def __init__(self):
        super().__init__()

        # Load the atlas
        self.image_width = 16
        self.image_height = 16
        self.image_scale_factor = 8
        self.animation = Animation("assets/bird_animation.png",
                                   size=(self.image_width, self.image_height),
                                   max_index=4, scale_factor=self.image_scale_factor)

        self.exercise_type = "squad"
        self.rand = random()

        self.rect = self.animation.image.get_rect()
        self.rect.bottomleft = (constants.WIDTH, constants.HEIGHT * 2 / 4)

    def move(self):
        """Move the enemy object."""
        self.rect.move_ip(-constants.SPEED, 0)
        self.animation.animate()

    def draw(self, surface):
        """Draw the enemy object on the surface."""
        surface.blit(self.animation.image, self.rect)

class Present():
    def __init__(self):
        """Initialize the enemy object."""
        super().__init__()

        # Load image
        self.image = pygame.image.load("assets/present.png")
        self.image_width = 32
        self.image_height = 32

        self.exercise_type = "reaction speed"
        self.rand = random()

        # Scale image
        self.image_scale_factor = 6
        self.image = pygame.transform.scale(self.image, (self.image_width * self.image_scale_factor,
                                                              self.image_height * self.image_scale_factor))

        self.rect = self.image.get_rect()
        self.rect.bottomleft = (constants.WIDTH, constants.HEIGHT * 3 / 4)

    def move(self):
        """Move the enemy object."""
        self.rect.move_ip(-constants.SPEED, 0)

    def draw(self, surface):
        """Draw the enemy object on the surface."""
        surface.blit(self.image, self.rect)


class Animation():
    def __init__(self, file_path, size, max_index, scale_factor=1):
        # set atlas parameters
        self.atlas_image = pygame.image.load(file_path).convert_alpha()
        self.atlas_index_max = max_index
        self.atlas_next_image = int(60 / 8)

        # set the width and height of the images from the atlas
        self.image_width = size[0]
        self.image_height = size[1]

        self.image_scale_factor = scale_factor

        self.atlas_index = 0
        self.refresh_timer = 0
        self.update_atlas()

    def update_atlas(self):
        x_offset = self.atlas_index * self.image_width
        self.image = pygame.Surface((self.image_width, self.image_height), pygame.SRCALPHA)
        self.image.blit(self.atlas_image, (0, 0), (x_offset, 0, self.image_width, self.image_height))
        self.image = pygame.transform.scale(self.image, (
        self.image_width * self.image_scale_factor, self.image_height * self.image_scale_factor))

    def next_atlas_image(self):
        if self.atlas_index + 1 >= self.atlas_index_max:
            self.atlas_index = 0
        else:
            self.atlas_index += 1

        self.update_atlas()

    def animate(self):
        self.refresh_timer += 1

        if self.refresh_timer >= self.atlas_next_image:
            self.next_atlas_image()
            self.refresh_timer = 0


def get_goal_jump_height():
    try:
        with open("assets/settings.json", "r") as file:
            setting = json.load(file)

        goal_jump_height = setting["Sprong oefening"]["Doel sprong hoogte (in cm)"]/100
        print("The goal jump height is " + str(goal_jump_height) + "m")

        return goal_jump_height
    except Exception as e:
        print("Could not get goal jump height:", e)
        return None


def init_image(path, dimension, scale_factor):
    temp_image = pygame.image.load(path).convert_alpha()
    return pygame.transform.scale(temp_image, (dimension[0] * scale_factor,
                                                    dimension[1] * scale_factor))


def append_squad_exercises_to_json(squad_data):
    # Load existing data from JSON file if it exists
    try:
        with open('assets/squad_data.json', 'r') as file:
            data = json.load(file)
    except FileNotFoundError:
        data = []

    # Append new data
    data.append({
        'date': datetime.now().strftime("%Y-%m-%d-%H-%M-%S"),
        'duration': squad_data["duration"],
        'average_speed': squad_data["max_speed"],
        'threshold_reached': squad_data["threshold_reached"]
    })

    # Write data back to JSON file
    with open('assets/squad_data.json', 'w') as file:
        json.dump(data, file, indent=4)


class Player(pygame.sprite.Sprite):
    """Player class represents the user-controlled character."""

    def __init__(self):
        """Initialize the player object."""
        super().__init__()

        # Load the atlas
        self.image_width = 32
        self.image_height = 64
        self.image_scale_factor = 4
        self.animation = Animation("assets/player_walk_animation.png",
                                   size=(self.image_width, self.image_height),
                                   max_index=5, scale_factor=self.image_scale_factor)

        self.rect = self.animation.image.get_rect()

        self.crouch = False

        # Set initial screen dimensions
        self.screen_width = constants.WIDTH
        self.screen_height = constants.HEIGHT

        # Scale factor for the player's image
        self.scale = 3
        self.rect = self.animation.image.get_rect()
        self.rect.bottomleft = (constants.WIDTH / 10, constants.HEIGHT * 3 / 4)

        self.jump_duration = 0

        self.jump_end_image = init_image("assets/player_end_jumping.png",
                                                [self.image_width, self.image_height+32],
                                                self.image_scale_factor)
        self.jump_start_image = init_image("assets/player_start_jumping.png",
                                            [self.image_width, self.image_height],
                                            self.image_scale_factor)
        self.crouch_image = init_image("assets/player_squat.png",
                                            [self.image_width, self.image_height],
                                            self.image_scale_factor)

        self.serial_jumping = False
        self.serial_squatting = False

        self.jumping = False
        self.animation_type = "walk"

        self.goal_jump_height = get_goal_jump_height()

    def get_collision_rect(self):
        collision_rect = pygame.rect.Rect((self.rect.topleft[0], 0), (self.rect.width, constants.HEIGHT))
        return collision_rect

    def append_jump_exercises_to_json(self, jump_data):
        # Load existing data from JSON file if it exists
        try:
            with open('assets/jump_data.json', 'r') as file:
                data = json.load(file)
        except FileNotFoundError:
            data = []

        # Append new data
        jump_height = 0.125 * 9.81 * (jump_data["duration"]/1000) ** 2
        data.append({
            'date': datetime.now().strftime("%Y-%m-%d-%H-%M-%S"),
            'duration': jump_data["duration"],
            'jump_height': jump_height
        })

        if self.goal_jump_height and jump_height >= self.goal_jump_height:
            hud.play_achievement()
            print("Jump height reached!")
        else:
            print(jump_height)

        # Write data back to JSON file
        with open('assets/jump_data.json', 'w') as file:
            json.dump(data, file, indent=4)

    def update_hardware_mode(self, display_surface, serial_input):
        """Update the player's position based on hardware input."""
        self.animation.animate()

        if serial_input and "Jump" in serial_input and serial_input["Jump"]:
            self.serial_jumping = True
        elif serial_input and "Jump" in serial_input and not serial_input["Jump"]:
            self.serial_jumping = False
            self.append_jump_exercises_to_json(serial_input)
        elif serial_input and "message" in serial_input and serial_input["message"] == "Starting a new squat!":
            self.serial_squatting = True
        elif serial_input and "message" in serial_input and serial_input["message"] == "Threshold reached!":
            hud.play_achievement()
        elif serial_input and "message" in serial_input and serial_input["message"] == "End of squad!":
            append_squad_exercises_to_json(serial_input)
            self.serial_squatting = False

        # Parse serial input
        if self.serial_jumping and not self.animation_type == "end_jump":
            jump_speed = -1 * 10 ** -1 * self.jump_duration ** 2. + 40  # f: y=-breete^(-1) x^(2)+hoogte

            if jump_speed > 0:
                self.rect.move_ip(0, -jump_speed)
                self.animation_type = "start_jump"
            else:
                self.rect.move_ip(0, 5)

                if self.rect.bottomleft[1] < constants.HEIGHT * 3 / 4:
                    self.animation_type = "end_jump"
                else:
                    self.animation_type = "run"
            self.jump_duration += 1
        else:
            self.jump_duration = 0
            self.rect.move_ip(0, 5)

            if self.rect.bottomleft[1] < constants.HEIGHT * 3 / 4:
                self.animation_type = "end_jump"
            else:
                self.animation_type = "run"

        if not self.serial_jumping and self.serial_squatting:
            self.animation_type = "crouch"

        # Ensure the player remains within the screen bounds
        playing_feeld = display_surface.get_rect()
        playing_feeld.bottomleft = (0, constants.HEIGHT * 3 / 4)
        self.rect.clamp_ip(playing_feeld)
    def update(self, display_surface):
        """Update the player's position based on user input."""
        pressed_keys = pygame.key.get_pressed()
        self.animation.animate()

        # Jump mechanic with space or up arrow
        if pressed_keys[pygame.K_SPACE] or pressed_keys[pygame.K_UP]:
            jump_speed = -1 * 100 ** -1 * self.jump_duration ** 2. + 17  # f: y=-breete^(-1) x^(2)+hoogte

            if jump_speed > 0:
                self.rect.move_ip(0, -jump_speed)
                self.animation_type = "start_jump"
            else:
                self.rect.move_ip(0, 5)

                if self.rect.bottomleft[1] < constants.HEIGHT * 3 / 4:
                    self.animation_type = "end_jump"
                else:
                    self.animation_type = "run"
            self.jump_duration += 1

        else:
            self.jump_duration = 0
            self.rect.move_ip(0, 5)

            if self.rect.bottomleft[1] < constants.HEIGHT * 3 / 4:
                self.animation_type = "end_jump"
            else:
                self.animation_type = "run"

        # Crouch action with down arrow
        if pressed_keys[pygame.K_DOWN] or pressed_keys[pygame.K_LCTRL] and not self.rect.bottomleft[1] < constants.HEIGHT * 3 / 4:
            self.animation_type = "crouch"

        # Ensure the player remains within the screen bounds
        playing_feeld = display_surface.get_rect()
        playing_feeld.bottomleft = (0, constants.HEIGHT * 3 / 4)
        self.rect.clamp_ip(playing_feeld)

    def draw(self, surface):
        """Draw the player object on the surface."""
        if self.animation_type == "crouch":
            surface.blit(self.crouch_image, self.rect)
        elif self.animation_type == "start_jump":
            surface.blit(self.jump_start_image, self.rect)
        elif self.animation_type == "end_jump":
            self.rect.y -= 32*self.image_scale_factor
            surface.blit(self.jump_end_image, self.rect)
            self.rect.y += 32*self.image_scale_factor
        else:
            surface.blit(self.animation.image, self.rect)
