import pygame
import constants

class Background:
    """Background class represents the game's background."""

    def __init__(self, start_position: int):
        super().__init__()

        """Initialize the background object."""
        self.image = pygame.image.load("assets/background.jpg")  # Load the background image
        self.rect = self.image.get_rect()  # Get the rectangle representing the dimensions of the image

        self.scale(constants.WIDTH, constants.HEIGHT)  # Scale the background image to fit the screen size
        self.rect.topleft = (start_position, 0)  # Set the initial position of the background

    def scale(self, screen_width, screen_height):
        self.image = pygame.transform.scale(  # Scale the background image to fit the screen size
            self.image, (screen_width, screen_height))

    def update(self):
        self.rect.move_ip(0, 0)  # Move the background to the left (creating a scrolling effect)
        if self.rect.topright[0] <= 0:  # If the right edge of the background moves off the screen, reset its position
            self.rect.topleft = (self.rect.width, 0)

    def draw(self, surface):
        """Draw the background on the surface."""
        surface.blit(self.image, self.rect)  # Blit (draw) the background image onto the surface at its rectangle
        # position


class ScrollingGround:
    def __init__(self):
        self.ground1 = Ground()
        self.ground2 = Ground()
        self.ground2.rect.topleft = [constants.WIDTH, constants.HEIGHT * 3 / 4]

    def update(self):
        self.ground1.rect.move_ip(-constants.SPEED, 0)
        self.ground2.rect.move_ip(-constants.SPEED, 0)

        if self.ground1.rect.topright[0] <= 0:
            self.ground1.rect.topleft = [constants.WIDTH, constants.HEIGHT * 3 / 4]

        if self.ground2.rect.topright[0] <= 0:
            self.ground2.rect.topleft = [constants.WIDTH, constants.HEIGHT * 3 / 4]

    def draw(self, surface):
        self.ground1.draw(surface)
        self.ground2.draw(surface)

class Ground:
    """Background class represents the game's ground."""

    def __init__(self):
        """Initialize the ground object."""
        # Load the ground image and set its initial position
        self.image = pygame.image.load("assets/ground.png")
        self.rect = self.image.get_rect()
        self.rect.topleft = (0, 0)

        # Scale the ground to fit the screen dimensions
        self.scale(constants.WIDTH, constants.HEIGHT)

    def scale(self, screen_width, screen_height):
        """Scale the ground image to fit the screen."""
        # Scale the ground image to a quarter of the screen's height and adjust its position
        self.image = pygame.transform.scale(self.image, (screen_width, screen_height / 4))
        self.rect = self.image.get_rect()
        self.rect.topleft = (0, screen_height * 3 / 4)  # Set the position at 3/4th down the screen

    def draw(self, surface):
        """Draw the ground on the given surface."""
        surface.blit(self.image, self.rect)  # Draw the ground image on the surface