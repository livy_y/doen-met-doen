import sys

import pygame
from game.hud import Text
import constants

class Done:
    def __init__(self, display_surface):
        self.display_surface = display_surface
        self.font = pygame.font.SysFont(constants.FONT, 64)
        self.text = self.font.render("Je hebt al je oefeningen gedaan!", True, constants.FONT_COLOR)
        self.text_rect = self.text.get_rect(center=self.display_surface.get_rect().center)
        self.clock = pygame.time.Clock()
        self.done = False

    def run(self):
        self.done = False
        start_time = pygame.time.get_ticks()

        self.display_surface.fill((0, 0, 0))
        self.display_surface.blit(self.text, self.text_rect)

        while not self.done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

            if pygame.time.get_ticks() - start_time >= 5000:  # 5000 milliseconds = 5 seconds
                self.done = True

            pygame.display.flip()
            self.clock.tick(60)