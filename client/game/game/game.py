import sys, os

# Add the parent directory to sys.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from datetime import datetime

import pygame
import game.entities as entities
import game.world as world
import game.hud as hud
from backend.UART import SerialPortListener
import constants
import json
import random

class Game:
    def __init__(self):
        pygame.init()
        self.display_surface = pygame.display.set_mode((constants.WIDTH, constants.HEIGHT))
        pygame.display.set_caption("Doen met doel")
        self.get_settings()
        self.clock = pygame.time.Clock()
        self.player = entities.Player()
        self.enemies = entities.EnemySpawner(self.squat_chance, self.jump_height_chance, self.reaction_speed_chance)
        self.background = world.Background(0)
        self.scrolling_ground = world.ScrollingGround()
        self.score = hud.Score()
        self.uart = SerialPortListener()

        self.uart_enabled = self.uart.is_enabled()
        self.reaction_speed = False
        self.running = True

    def get_settings(self):
        # Read settings.json
        with open('assets/settings.json') as f:
            settings = json.load(f)

        # Extract the value of "aantal oefeningen"
        self.score_goal = settings["oefeningen"]["aantal oefeningen"]
        self.squat_chance = settings["Percentuele kans"]["Squat"]
        self.jump_height_chance = settings["Percentuele kans"]["Spronghoogte"]
        self.reaction_speed_chance = settings["Percentuele kans"]["Reactie snelheid"]

        # Now you can use score_goal variable in your main.py
        print("The score goal is:", self.score_goal)

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
        return True

    def update(self):
        serial_input = self.uart.get_processed_messages()

        if self.uart_enabled:
            self.player.update_hardware_mode(self.display_surface, serial_input)
        else:
            self.player.update(self.display_surface)
        self.background.update()
        self.enemies.spawn()
        self.enemies.move()
        self.scrolling_ground.update()

        collision = self.enemies.check_collision(self.player.get_collision_rect())
        if collision:
            if collision == "jump" and self.player.animation_type == "start_jump" or self.player.animation_type == "end_jump":
                hud.play_good_sound()
                self.score.add()
            elif collision == "jump":
                hud.play_bad_sound()
            elif collision == "squad" and self.player.animation_type == "crouch":
                hud.play_good_sound()
                self.score.add()
            elif collision == "squad":
                hud.play_bad_sound()

            elif collision == "reaction speed":
                print("Testing reaction speed!")
                self.reaction_speed_exercise(times=3)
                self.score.add()

        if self.score.score >= self.score_goal:
            print("The goal has been reached!")
            self.append_exercises_to_json()
            return "break"

    def reaction_speed_exercise(self, times):
        # Create black squares (adjust positions as needed)
        square_size = 300  # Modify this to change the size of the squares
        square_color = (0, 0, 0)  # Change this to adjust the color

        center_x = constants.WIDTH // 2
        center_y = constants.HEIGHT // 2

        mid_rect = pygame.Rect(center_x - square_size // 2, center_y - square_size // 2, square_size,
                                square_size)
        left_rect = pygame.Rect(center_x - square_size // 2 - square_size * 1.5, center_y - square_size // 2,
                               square_size, square_size)
        right_rect = pygame.Rect(center_x - square_size // 2 + square_size * 1.5, center_y - square_size // 2,
                                 square_size, square_size)

        # Add dark transparency
        background = pygame.Surface((constants.WIDTH, constants.HEIGHT))
        background.set_alpha(128)
        background.fill((0, 0, 0))
        self.display_surface.blit(background, (0, 0))

        rects = [left_rect, mid_rect, right_rect]
        index_to_button = {
            0: "left_button",
            1: "middle_button",
            2: "right_button"
        }

        previous_highlighted_button = random.randrange(0, 3)

        for time in range(times):
            highlighted_button = random.randrange(0, 3)

            if previous_highlighted_button and highlighted_button == previous_highlighted_button:
                highlighted_button += 1

                if highlighted_button > 2:
                    highlighted_button = 0
            else:
                previous_highlighted_button = highlighted_button

            for index, rect in enumerate(rects):
                if highlighted_button == index:
                    pygame.draw.rect(self.display_surface, constants.FONT_COLOR, rect)
                else:
                    pygame.draw.rect(self.display_surface, constants.BACKGROUND_COLOR, rect)

            pygame.display.flip()
            start_time = time.time()

            waiting = True

            while waiting and self.running:
                self.running = self.handle_events()
                serial_input = self.uart.get_processed_messages()

                if serial_input and index_to_button[highlighted_button] in serial_input:
                    waiting = False

            if not self.running:
                break

            end_time = time.time()
            self.append_reaction_speed_to_json(start_time, end_time)

    def append_reaction_speed_to_json(self, start_time, end_time):
        try:
            with open('assets/reaction_speed_data.json', 'r') as file:
                data = json.load(file)
        except FileNotFoundError:
            data = []

        # Append new data
        data.append({
            'date': datetime.now().strftime("%Y-%m-%d-%H-%M-%S"),
            'duration': end_time-start_time
        })

        # Write data back to JSON file
        with open('assets/reaction_speed_data.json', 'w') as file:
            json.dump(data, file, indent=4)


    def append_exercises_to_json(self):
        # Load existing data from JSON file if it exists
        try:
            with open('assets/exercise_data.json', 'r') as file:
                data = json.load(file)
        except FileNotFoundError:
            data = []

        # Append new data
        data.append({
            'date': datetime.now().strftime("%Y-%m-%d"),
            'exercises': self.score_goal
        })

        # Write data back to JSON file
        with open('assets/exercise_data.json', 'w') as file:
            json.dump(data, file, indent=4)

    def display_goal(self):
        self.display_surface.fill(((255, 255, 255)))

    def draw(self):
        self.display_surface.fill((0, 0, 0))
        self.background.draw(self.display_surface)
        self.scrolling_ground.draw(self.display_surface)
        self.player.draw(self.display_surface)
        self.enemies.draw(self.display_surface)
        self.score.draw(self.display_surface)
        pygame.display.flip()

    def run(self):
        result = self.uart.find_serial_port()

        if result == "The device is not connected.":
            self.uart_enabled = False
        elif result == "The device is connected.":
            self.uart_enabled = True

        self.running = True
        while self.running:
            self.running = self.handle_events()
            state = self.update()
            self.draw()
            self.clock.tick(constants.FPS)

            if state == "break":
                break
