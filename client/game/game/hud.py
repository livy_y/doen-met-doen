import pygame
import constants


def play_sound(sound_file):
    pygame.mixer.init()
    pygame.mixer.music.load(sound_file)
    pygame.mixer.music.play()


def play_good_sound():
    play_sound("assets/good.ogg")


def play_bad_sound():
    play_sound("assets/bad.ogg")


def play_achievement():
    play_sound("assets/complete.ogg")


class Score:
    def __init__(self):
        self.score = 0
        self.prefix = "Aantal oefeningen: "
        self.text = Text("")
        self.update_text()

    def update_text(self):
        self.text.update(str(self.prefix + str(self.score)))

    def add(self):
        self.score += 1
        self.update_text()

    def draw(self, surface):
        self.text.draw(surface)


class Text:
    def __init__(self, display_text, color=None, size=32):
        self.rect = pygame.Rect(0, 0, 40, 30)  # creating a rectangle for the text

        if color:
            self.color = color  # choosing a color for the text
        else:
            self.color = constants.FONT_COLOR

        self.font = pygame.font.Font(constants.FONT, size)  # choosing of font and size of the text
        self.update(display_text)

    def update(self, display_text):
        self.img = self.font.render(display_text, True, self.color)  # choosing of color and what to write

    def draw(self, surface):
        surface.blit(self.img, self.rect)
