# Doen Met Doen

## Overview

This project was developed as part of the participation of the 6th-grade class from Heilig Hart Heverlee in the Stem Boost Tournament organized by Uhasselt and PXL. Our goal was to develop technological solutions for rehabilitation exercises. Specifically, we worked on three exercises: jumping, squats, and reaction speed.

### Selected Exercises

- **Squats**: We measure the squat movement using an MPU6050 for rotation and an ESP32.
- **Jumping**: Jump height is measured using a scale. We calculate the force to determine the jump height.
- **Reaction Speed**: For this component, we use buttons and boxes on the screen. When a box is filled, you need to press the corresponding button as quickly as possible.

### Game Development

We developed a game similar to the Chrome Dino game where you receive live feedback on your performance in the various exercises.

## File Structure

- **/hardware**
  - *Bracelet*: Code for the ESP32 connected to the MPU6050.
  - *Commander*: Code for the ESP32 connected to the scale, buttons for reaction speed, and the PC via USB.
- **/client**
  - Software that will run on the pc and communicate with the hardware.

## Authors

- Dorien
- Liv

## Credit

- Thank Noor for helping with the art assets!
- Thank [Baŝto](https://opengameart.org/users/ba%C5%9Dto) for the sound effect of the achievement!

## License

This project is licensed under the GNU Affero General Public License v3.0. See [license](LICENSE) for details.
