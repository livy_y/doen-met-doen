import network


def connect(ssid: str, password: str):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(ssid, password)
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())


def scan():
    wlan = network.WLAN(network.STA_IF)
    wlan.active
    res = wlan.scan()

    print("The following ssid's have been found")
    for ssid in res:
        print(ssid)


def get_mac():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    print("the mac addres is " + str(wlan.config("mac")))


def save_mac(file: str):
    wlan = network.WLAN(network.STA_IF)
    mac = wlan.config("mac")
    print("The mac addres is " + str(mac))

    file = open(file, "w")
    file.write(str(mac))
    file.close()
