from mpu6050_light import MPU6050_LIGHT
from utime import sleep, ticks_ms, ticks_diff
from network import WLAN, STA_IF
from espnow import ESPNow
from machine import Pin
import json

class SquatDetector:
    """
    SquatDetector class implements a simple squat detection algorithm using an MPU6050 sensor.

    Attributes:
        mpu (MPU6050_LIGHT): Instance of the MPU6050_LIGHT class for sensor interaction.
        start_time (int): Time of squat initiation.
        squat_detected (bool): Flag indicating if a squat has been detected.
        threshold_reached (bool): Flag indicating if the end threshold of the squat has been reached.
        squat_duration (int): Duration of the detected squat.
        max_squat_speed (float): Maximum speed during the squat movement.
        start_threshold (int): Threshold for detecting the start of a squat.
        end_threshold (int): Threshold for detecting the end of a squat.
        calibration_offset (int): Offset so that the standing position has an x angle of zero.
    """

    def __init__(self):
        """
        Initializes and calibrates the MPU6050 sensor for squat detection.
        """
        self.mpu = MPU6050_LIGHT()
        self.start_time = 0
        self.squat_detected = False
        self.threshold_reached = False
        self.squat_duration = 0
        self.max_squat_speed = 0
        self.start_threshold = 15
        self.end_threshold = 40
        self.calibration_offset = 0

    def setup(self):
        print("Setup MPU6050")
        self.mpu.begin()

        print("Calculating offsets, do not move MPU6050")
        sleep(1)
        self.mpu.calcOffsets()  # gyro and accelero
        print("Done!\n")

    def calibrate_sensor(self):
        # Method to calibrate the sensor by setting x-angle to zero
        self.calibration_offset = self.mpu.angleX
        print("Sensor calibrated. X-angle set to zero.")

    def set_end_threshold(self):
        # Method to set the end threshold
        self.end_threshold = self.get_rotation()
        print(f"End threshold set to {self.get_rotation()}.")

    def get_rotation(self):
        return abs(self.mpu.angleX - self.calibration_offset)

    def loop(self):
        """
        Continuously checks for squat movements and calculates squat-related metrics.
        Returns a message indicating the detected squat information.
        """
        self.mpu.update()
        print("X:{:.2f}, Y:{:.2f}".format(self.get_rotation(), self.mpu.angleY))

        if not self.squat_detected and self.get_rotation() > self.start_threshold:
            self.start_time = ticks_ms()
            self.squat_detected = True

            data = {
                "message": "Starting a new squat!"
            }
            parsed = json.dumps(data)
            print(parsed)
            return parsed

        elif self.squat_detected and not self.threshold_reached and self.get_rotation() > self.end_threshold:
            self.threshold_reached = True

            self.squat_duration = ticks_ms() - self.start_time
            self.max_squat_speed = abs(self.get_rotation()) / (self.squat_duration / 1000)

            data = {
                "message": "Threshold reached!",
                "duration": self.squat_duration,
                "max_speed": round(self.max_squat_speed, 2)
            }
            parsed = json.dumps(data)
            print(parsed)
            return parsed

        elif self.squat_detected and self.get_rotation() < self.start_threshold and (ticks_ms() - self.start_time) > 1000:
            self.squat_duration = ticks_ms() - self.start_time
            self.max_squat_speed = abs(self.get_rotation()) / (self.squat_duration / 1000)

            data = {
                "message": "End of squad!",
                "duration": self.squat_duration,
                "max_speed": round(self.max_squat_speed, 2),
                "threshold_reached": self.threshold_reached
            }
            parsed = json.dumps(data)
            print(parsed)

            self.threshold_reached = False
            self.squat_detected = False

            return parsed

        return None


class EspnowHandler:
    """
    EspnowHandler class handles ESPNow communication with a specific MAC address.

    Attributes:
        commander_mac (bytes): MAC address of the ESP32 commander device.
        now (ESPNow): ESPNow communication instance.
    """

    def __init__(self, commander_mac):
        self.commander_mac = commander_mac

        # A WLAN interface must be active to send()/recv()
        sta = WLAN(STA_IF)  # Or network.AP_IF
        sta.active(True)
        sta.disconnect()  # For ESP8266

        # Activate the ESPNow communication protocol
        self.now = ESPNow()
        self.now.active(True)
        self.now.add_peer(self.commander_mac)  # Must add_peer() before send()
        self.now.send(self.commander_mac, "tesssssst")

    """ Sends data to the specified MAC address using ESPNow communication protocol. """

    def send(self, data: str):
        self.now.send(self.commander_mac, b'start')
        self.now.send(self.commander_mac, data, True)
        self.now.send(self.commander_mac, b'end')


# Define GPIO pins numbers for the buttons
calibration_button_pin = Pin(2, Pin.IN, Pin.PULL_UP)
threshold_button_pin = Pin(15, Pin.IN, Pin.PULL_UP)
led_pin = Pin(23, Pin.OUT)

# Initialize SquatDetector with threshold values
squat_detector = SquatDetector()
squat_detector.setup()

COMMANDE_MAC = b'\xa0\xb7eV\xcd\x98'

now = EspnowHandler(COMMANDE_MAC)
now.send("Esp is starting up")

# Timer setup
last_run_time = ticks_ms()
interval = 100  # Set the interval in milliseconds (e.g., 5000ms = 5 seconds)

# Turn power led on
led_pin.value(1)

# Run loop continuously
while True:
    # Check for button presses
    if not calibration_button_pin.value():
        led_pin.value(0)
        # Activate calibration method when the calibration button is pressed
        squat_detector.calibrate_sensor()
        sleep(0.5)  # Add a small delay to prevent multiple rapid triggers
        led_pin.value(1)

    if not threshold_button_pin.value():
        led_pin.value(0)
        # Activate threshold method when the threshold button is pressed
        squat_detector.set_end_threshold()  # Example threshold value, change as needed
        sleep(0.5)  # Add a small delay to prevent multiple rapid triggers
        led_pin.value(1)
        sleep(0.5)  # Add a small delay to show that you are calibrating
        led_pin.value(0)
        sleep(0.5)  # Add a small delay to show that you are calibrating
        led_pin.value(1)

    # Check if the interval has passed before running the squat detection code
    if ticks_diff(ticks_ms(), last_run_time) >= interval:
        last_run_time = ticks_ms()  # Update the last run time
        squat_data = squat_detector.loop()

        if squat_data:
            now.send(squat_data)
