from machine import Pin, SoftI2C
from math import atan2, pi, sqrt
from utime import sleep_ms, ticks_ms

"""
This is a translation of the c++ MPU6050_LIGHT created by rfetick to micropython.

Attributes of the MPU6050_LIGHT library:
        MPU6050_ADDR (int): I2C address of the MPU6050 sensor.
        MPU6050_SMPLRT_DIV_REGISTER (int): Sample Rate Divider register address.
        MPU6050_CONFIG_REGISTER (int): Configuration register address.
        MPU6050_GYRO_CONFIG_REGISTER (int): Gyroscope configuration register address.
        MPU6050_ACCEL_CONFIG_REGISTER (int): Accelerometer configuration register address.
        MPU6050_PWR_MGMT_1_REGISTER (int): Power Management 1 register address.
        MPU6050_GYRO_OUT_REGISTER (int): Gyroscope output register address.
        MPU6050_ACCEL_OUT_REGISTER (int): Accelerometer output register address.
        RAD_2_DEG (float): Conversion factor for radians to degrees.
        CALIB_OFFSET_NB_MES (int): Number of calibration offset measurements.
        TEMP_LSB_2_DEGREE (float): Temperature conversion factor from bit to Celsius.
        TEMP_LSB_OFFSET (float): Temperature offset value in bits.
"""

# The register map is provided at: https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Register-Map1.pdf
MPU6050_ADDR = 0x68  # 0x68 or 0x69
MPU6050_SMPLRT_DIV_REGISTER = 0x19
MPU6050_CONFIG_REGISTER = 0x1a
MPU6050_GYRO_CONFIG_REGISTER = 0x1b
MPU6050_ACCEL_CONFIG_REGISTER = 0x1c
MPU6050_PWR_MGMT_1_REGISTER = 0x6b

MPU6050_GYRO_OUT_REGISTER = 0x43
MPU6050_ACCEL_OUT_REGISTER = 0x3B

RAD_2_DEG = 57.29578  # [deg/rad]
CALIB_OFFSET_NB_MES = 500
TEMP_LSB_2_DEGREE = 340.0  # [bit/celsius]
TEMP_LSB_OFFSET = 12412.0

DEFAULT_GYRO_COEFF = 0.98


def wrap(angle: float, limit: float) -> float:
    # Ensure angle stays within the specified limit
    while angle > limit:
        angle -= 2 * limit  # Subtract 2 times the limit until angle is within the limit
    while angle < -limit:
        angle += 2 * limit  # Add 2 times the limit until angle is within the limit

    return angle  # Return the angle adjusted to stay within the limit


class MPU6050_LIGHT:
    """
    MPU6050_LIGHT class provides access to MPU6050 sensor functionalities.

    This class allows interaction with the MPU6050 sensor via I2C connection.
    The MPU6050 sensor is capable of providing accelerometer, gyroscope, and temperature data.
    """

    def __init__(self, scl_pin: int = 22, sda_pin: int = 21):
        """
        Initializes the MPU6050 sensor object.
        """

        self.iic = SoftI2C(scl=Pin(scl_pin), sda=Pin(sda_pin), freq=100000)

        # Setup i2c connection
        self.iic.start()
        self.iic.writeto(MPU6050_ADDR, bytearray([107, 0]))
        self.iic.stop()

        self.gyro_lsb_to_degsec = 131.0
        self.acc_lsb_to_g = 16384.0

        self.gyroXoffset = 0
        self.gyroYoffset = 0
        self.gyroZoffset = 0

        self.accXoffset = 0
        self.accYoffset = 0
        self.accZoffset = 0

        self.temp = 0
        self.accX = 0
        self.accY = 0
        self.accZ = 0

        self.gyroX = 0
        self.gyroY = 0
        self.gyroZ = 0
        self.angleAccX = 0
        self.angleAccY = 0

        self.angleX = 0
        self.angleY = 0
        self.angleZ = 0

        self.preInterval = ticks_ms()
        self.filterGyroCoef = 0  # complementary filter coefficient to balance gyro vs accelero data to get angle

    def begin(self, gyro_config_num: int = 1, acc_config_num: int = 0):
        """
        Initializes the sensor with specified configurations for gyroscope and accelerometer.
        """

        self.writeData(MPU6050_PWR_MGMT_1_REGISTER, 0x01)  # check only the first connection with status
        self.writeData(MPU6050_SMPLRT_DIV_REGISTER, 0x00)
        self.writeData(MPU6050_CONFIG_REGISTER, 0x00)

        self.setGyroConfig(gyro_config_num)
        self.SetAccConfig(acc_config_num)

        self.update()

    def writeData(self, reg, data):
        """
        Writes data to a specific register in the MPU6050 sensor.
        """

        self.iic.start()
        self.iic.write(bytes([MPU6050_ADDR << 1]))
        self.iic.write(bytes([reg, data]))
        self.iic.stop()

    def calcOffsets(self, is_calc_gyro: bool = True, is_calc_acc: bool = True):
        """
        Calculates the offsets for gyroscope and/or accelerometer.
        """

        # Check if gyro offset calculation is requested, set initial gyro offsets to zero
        if is_calc_gyro:
            self.setGyroOffsets(0, 0, 0)

        # Check if accelerometer offset calculation is requested, set initial acc offsets to zero
        if is_calc_acc:
            self.setAccOffsets(0, 0, 0)

        ag = [0, 0, 0, 0, 0, 0]  # Initialize accumulator for 3*acc, 3*gyro

        # Iterate for calibration offset measurements
        for i in range(0, CALIB_OFFSET_NB_MES):
            # Fetch sensor data for calibration
            self.fetchData()

            # Accumulate accelerometer and gyroscope values
            ag[0] += self.accX
            ag[1] += self.accY
            ag[2] += (self.accZ - 1.0)  # Subtract 1g (gravity) from Z-axis for calibration
            ag[3] += self.gyroX
            ag[4] += self.gyroY
            ag[5] += self.gyroZ
            sleep_ms(1)  # Wait between measurements for stability

        # Calculate average offsets if requested for accelerometer calibration
        if is_calc_acc:
            accXoffset = ag[0] / CALIB_OFFSET_NB_MES
            accYoffset = ag[1] / CALIB_OFFSET_NB_MES
            accZoffset = ag[2] / CALIB_OFFSET_NB_MES

        # Calculate average offsets if requested for gyroscope calibration
        if is_calc_gyro:
            gyroXoffset = ag[3] / CALIB_OFFSET_NB_MES
            gyroYoffset = ag[4] / CALIB_OFFSET_NB_MES
            gyroZoffset = ag[5] / CALIB_OFFSET_NB_MES

    def calcGyroOffsets(self):
        """
        Calculates the offsets for the gyroscope.
        """

        self.calcOffsets(True, False)

    def calcAccOffsets(self):
        """
        Calculates the offsets for the accelerometer.
        """

        self.calcOffsets(False, True)

    def setGyroConfig(self, config_num: int):
        """
        Sets the configuration for the gyroscope range.
        """

        if config_num == 0:
            # range = +- 250 deg/s
            self.writeData(MPU6050_GYRO_CONFIG_REGISTER, 0x00)
        elif config_num == 1:
            # range = +- 500 deg/s
            self.writeData(MPU6050_GYRO_CONFIG_REGISTER, 0x08)
        elif config_num == 2:
            # range = +- 1000 deg/s
            self.writeData(MPU6050_GYRO_CONFIG_REGISTER, 0x10)
        elif config_num == 3:
            # range = +- 2000 deg/s
            self.writeData(MPU6050_GYRO_CONFIG_REGISTER, 0x18)

    def SetAccConfig(self, config_num: int):
        """
        Sets the configuration for the accelerometer range.
        """

        if config_num == 0:
            # range = +- 2 g
            self.writeData(MPU6050_ACCEL_CONFIG_REGISTER, 0x00)
        if config_num == 1:
            # range = +- 4 g
            self.writeData(MPU6050_ACCEL_CONFIG_REGISTER, 0x08)
        if config_num == 2:
            # range = +- 8 g
            self.writeData(MPU6050_ACCEL_CONFIG_REGISTER, 0x10)
        if config_num == 3:
            # range = +- 16 g
            self.writeData(MPU6050_ACCEL_CONFIG_REGISTER, 0x18)

    def setGyroOffsets(self, x_offset: float, y_offset: float, z_offset: float):
        """
        Sets the offsets for the gyroscope.
        """

        self.gyroXoffset = x_offset
        self.gyroYoffset = y_offset
        self.gyroZoffset = z_offset

    def setAccOffsets(self, x_offset: float, y_offset: float, z_offset: float):
        """
        Sets the offsets for the accelerometer.
        """

        self.accXoffset = x_offset
        self.accYoffset = y_offset
        self.accZoffset = z_offset

    def setFilterGyroCoef(self, gyro_coeff: float):
        """
        Sets the coefficient for the gyro filter.
        """

        if gyro_coeff < 0 or gyro_coeff > 1:
            gyro_coeff = DEFAULT_GYRO_COEFF  # prevent bad gyro coeff
        self.filterGyroCoef = gyro_coeff

    def setFilterAccCoef(self, acc_coeff: float):
        """
        Sets the coefficient for the accelerometer filter.
        """

        self.setFilterGyroCoef(1.0 - acc_coeff)

    def get_raw_values(self):
        """
        Retrieves raw sensor data from the MPU6050 sensor.
        """

        self.iic.start()
        res = self.iic.readfrom_mem(MPU6050_ADDR, MPU6050_ACCEL_OUT_REGISTER, 14)
        self.iic.stop()
        return res

    def _bytes_toint(self, firstbyte, secondbyte):
        """
        Converts two bytes to an integer.
        """

        if not firstbyte & 0x80:
            return firstbyte << 8 | secondbyte
        return - (((firstbyte ^ 255) << 8) | (secondbyte ^ 255) + 1)

    def fetchData(self):  # user should better call 'update' that includes 'fetchData'
        """
        Fetches sensor data from the MPU6050 sensor.
        """

        # Retrieve raw sensor data from the MPU6050 sensor
        rawData = self.get_raw_values()

        # Calculate accelerometer values and apply offsets
        self.accX = (self._bytes_toint(rawData[0], rawData[1])) / self.acc_lsb_to_g - self.accXoffset
        self.accY = (self._bytes_toint(rawData[2], rawData[3])) / self.acc_lsb_to_g - self.accYoffset
        self.accZ = (self._bytes_toint(rawData[4], rawData[5])) / self.acc_lsb_to_g - self.accZoffset

        # Calculate temperature in Celsius and apply offset conversion
        self.temp = (self._bytes_toint(rawData[6], rawData[7]) + TEMP_LSB_OFFSET) / TEMP_LSB_2_DEGREE

        # Calculate gyroscope values and apply offsets
        self.gyroX = (self._bytes_toint(rawData[8], rawData[9])) / self.gyro_lsb_to_degsec - self.gyroXoffset
        self.gyroY = (self._bytes_toint(rawData[10], rawData[11])) / self.gyro_lsb_to_degsec - self.gyroYoffset
        self.gyroZ = (self._bytes_toint(rawData[12], rawData[13])) / self.gyro_lsb_to_degsec - self.gyroZoffset

    def update(self):
        """
        Updates sensor data and calculates tilt angles using gyroscope and accelerometer data.
        """

        # retrieve raw data
        self.fetchData()

        # estimate tilt angles: this is an approximation for small angles!
        sgZ = -1 if self.accZ < 0 else 1  # allow one angle to go from -180 to +180 degrees

        # Calculate angles based on accelerometer data
        self.angleAccX = atan2(self.accY,
                               sgZ * sqrt(self.accZ * self.accZ + self.accX * self.accX)) * RAD_2_DEG  # [-180,+180] deg
        self.angleAccY = - atan2(self.accX,
                                 sqrt(self.accZ * self.accZ + self.accY * self.accY)) * RAD_2_DEG  # [- 90,+ 90] deg

        # Calculate time difference
        Tnew = ticks_ms()
        dt = (Tnew - self.preInterval) * 1e-3  # Calculate time difference in seconds
        self.preInterval = Tnew  # Update previous interval

        # Apply complementary filter to combine gyro and accelerometer data for angle calculation
        # X-axis angle
        self.angleX = wrap(
            self.filterGyroCoef * (self.angleAccX + wrap(self.angleX + self.gyroX * dt - self.angleAccX, 180)) + (
                    1.0 - self.filterGyroCoef) * self.angleAccX, 180)
        # Y-axis angle
        self.angleY = wrap(
            self.filterGyroCoef * (self.angleAccY + wrap(self.angleY + sgZ * self.gyroY * dt - self.angleAccY, 90)) + (
                    1.0 - self.filterGyroCoef) * self.angleAccY, 90)
        self.angleZ += self.gyroZ * dt  # Update Z-axis angle using only the gyroscope
