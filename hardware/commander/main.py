import _thread
import threads

# Create a lock for synchronized printing
print_lock = _thread.allocate_lock()

# Create instances of the classes with the print lock
esp_receiver = threads.ESPNowReceiver(print_lock)
another_instance = threads.ComponentThread(print_lock)

# Start listening for messages in a separate thread
_thread.start_new_thread(esp_receiver.start_listening, ())
# Start doing something in another class in a separate thread
_thread.start_new_thread(another_instance.loop, ())
