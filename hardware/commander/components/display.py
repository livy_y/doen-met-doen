from time import sleep
from ssd1306 import SSD1306_I2C
from math import sin, cos, radians, sqrt
from machine import SoftI2C, Pin

def hsv_to_rgb(h, s, v):
    """
    Convert HSV color to RGB.
    :param h: Hue value (0.0 to 1.0)
    :param s: Saturation value (0.0 to 1.0)
    :param v: Value (brightness) value (0.0 to 1.0)
    :return: RGB color tuple (values in the range 0 to 255)
    """
    if s == 0.0:
        # Achromatic (gray)
        return int(v * 255), int(v * 255), int(v * 255)

    h *= 6.0  # Sector 0 to 5
    i = int(h)
    f = h - i
    p = int(255 * v * (1.0 - s))
    q = int(255 * v * (1.0 - s * f))
    t = int(255 * v * (1.0 - s * (1.0 - f)))

    if i == 0:
        return int(v * 255), t, p
    elif i == 1:
        return q, int(v * 255), p
    elif i == 2:
        return p, int(v * 255), t
    elif i == 3:
        return p, q, int(v * 255)
    elif i == 4:
        return t, p, int(v * 255)
    else:
        return int(v * 255), p, q

def rotation_matrix_around_y(point, angle):
    rotation_matrix = [
        [cos(angle), -sin(angle)],
        [sin(angle), cos(angle)]
    ]

    return [
        point[0] * rotation_matrix[0][0] + point[2] * rotation_matrix[0][1],
        point[1],
        point[0] * rotation_matrix[1][0] + point[2] * rotation_matrix[1][1]
    ]
def isometric_projection(point):
    # Define isometric transformation matrix
    transformation_matrix = [
        [sqrt(2) / 2, -sqrt(2) / 2, 0],
        [1 / sqrt(6), 1 / sqrt(6), -2 / sqrt(6)],
        [1 / sqrt(6), 1 / sqrt(6), 1 / sqrt(6)]
    ]

    # Apply the transformation
    isometric_point = [
        point[0] * transformation_matrix[0][0] + point[1] * transformation_matrix[0][1] + point[2] *
        transformation_matrix[0][2],
        point[0] * transformation_matrix[1][0] + point[1] * transformation_matrix[1][1] + point[2] *
        transformation_matrix[1][2],
        point[0] * transformation_matrix[2][0] + point[1] * transformation_matrix[2][1] + point[2] *
        transformation_matrix[2][2]
    ]

    return isometric_point


def vector_multiplier(point, scalar):
    return [point[0] * scalar, point[1] * scalar, point[2] * scalar]

class Display:
    def __init__(self, ledring):
        ### INIT DISPLAY ###
        self.width = 128
        self.height = 64

        # Save ledring for use in intro
        self.led_ring = ledring

        # Create SSD1306 object
        self.i2c = SoftI2C(scl=Pin(22), sda=Pin(21), freq=400000)
        self.display = SSD1306_I2C(self.width, self.height, self.i2c)
        self.animate_3d_cube_on_oled(0.02)

        self.center_text("WELCOME")

    def text(self, x, y, text):
        self.display.text(text, int(x), int(y))
        self.display.show()

    def center_text(self, text):
        self.display.fill(0)
        self.display.show()

        self.display.text(text, int(128/2 - 8 * (len(text)/2)), int(64/2 - 4))
        self.display.show()

    def rotate_wireframe(self, size=20, speed=0.1):
        angle = 0
        while angle < 360:
            x1 = int(size * cos(radians(angle)))
            y1 = int(size * sin(radians(angle)))

            x2 = int(size * cos(radians(angle + 90)))
            y2 = int(size * sin(radians(angle + 90)))

            x3 = int(size * cos(radians(angle + 180)))
            y3 = int(size * sin(radians(angle + 180)))

            x4 = int(size * cos(radians(angle + 270)))
            y4 = int(size * sin(radians(angle + 270)))

            self.display.fill(0)
            self.display.line(x1 + self.width // 2, y1 + self.height // 2, x2 + self.width // 2, y2 + self.height // 2,
                              1)
            self.display.line(x2 + self.width // 2, y2 + self.height // 2, x3 + self.width // 2, y3 + self.height // 2,
                              1)
            self.display.line(x3 + self.width // 2, y3 + self.height // 2, x4 + self.width // 2, y4 + self.height // 2,
                              1)
            self.display.line(x4 + self.width // 2, y4 + self.height // 2, x1 + self.width // 2, y1 + self.height // 2,
                              1)
            self.display.show()

            time.sleep(speed)
            angle += 1

    def animate_3d_cube_on_oled(self, rotation_speed=1):
        cube_vertices = [
            [-1, -1, -1],
            [-1, -1, 1],
            [-1, 1, 1],
            [-1, 1, -1],
            [1, -1, -1],
            [1, -1, 1],
            [1, 1, 1],
            [1, 1, -1]
        ]
        cube_vertices = [vector_multiplier(point, 3) for point in cube_vertices]

        frames = 20
        for frame in range(frames):
            self.display.fill(0)
            self.display.show()

            angle = frame * rotation_speed

            cube_vertices = [rotation_matrix_around_y(point, angle) for point in cube_vertices]
            cube_vertices = [isometric_projection(point) for point in cube_vertices]

            for face in [
                [cube_vertices[0], cube_vertices[1], cube_vertices[2], cube_vertices[3]],
                [cube_vertices[4], cube_vertices[5], cube_vertices[6], cube_vertices[7]],
                [cube_vertices[0], cube_vertices[1], cube_vertices[5], cube_vertices[4]],
                [cube_vertices[2], cube_vertices[3], cube_vertices[7], cube_vertices[6]],
                [cube_vertices[1], cube_vertices[2], cube_vertices[6], cube_vertices[5]],
                [cube_vertices[0], cube_vertices[3], cube_vertices[7], cube_vertices[4]]
            ]:
                for i in range(4):
                    x1, y1, _ = face[i]
                    x2, y2, _ = face[(i + 1) % 4]
                    x1, y1 = int(x1 * 10) + self.width // 2, int(y1 * 10) + self.height // 2
                    x2, y2 = int(x2 * 10) + self.width // 2, int(y2 * 10) + self.height // 2
                    self.display.line(x1, y1, x2, y2, 0xffff)

                    # Set the color of corresponding LED based on the position in the circle

                    # Calculate the dynamic index for each LED by combining the frame and vertex index
                    # The frame / frames factor ensures a smooth transition over the animation frames
                    # The % self.NUM_LEDS ensures the index stays within the bounds of the LED ring
                    index = int(frame / frames * self.led_ring.NUM_LEDS)

                    # Map the index to a hue value within the range [0.0, 1.0]
                    hue = (index / self.led_ring.NUM_LEDS) % 1.0

                    # Convert the hue value to an RGB color using the hsv_to_rgb function
                    # Saturation and value are set to 1.0 for vivid and bright colors
                    rgb_color = [int(c * 255) for c in hsv_to_rgb(hue, 1.0, 1.0)]

                    # Set the calculated RGB color to the corresponding LED in the LED ring
                    self.led_ring.set_led_color(index, rgb_color)
            self.display.show()
            sleep(0.05)

        self.led_ring.set_color((0, 0, 255))
        self.display.fill(0)
        self.display.show()
