import time
from neopixel import NeoPixel
from machine import Pin
import json

class Buttons:
    def __init__(self, button_pins, button_names):
        self.buttons = [Button(pin) for pin in button_pins]
        self.button_names = button_names

    def update(self):
        pressed_buttons = []
        for i, button in enumerate(self.buttons):
            if button.is_pressed():  # button pressed (assuming active low)
                pressed_buttons.append(i)
        return pressed_buttons

    def is_pressed(self, button_index):
        return self.buttons[button_index].is_pressed()

    def any_pressed(self):
        return any(button.is_pressed() for button in self.buttons)

    @micropython.native
    def get_pressed_buttons_json(self):
        pressed_buttons = self.update()
        if not pressed_buttons:
            return ""
        button_states = {self.button_names[self.buttons[i].pin]: True for i in pressed_buttons}
        return json.dumps(button_states)


class Button:
    def __init__(self, pin):
        self.pin = pin
        self.button = Pin(pin, Pin.IN, Pin.PULL_UP)

    def is_pressed(self):
        return not self.button.value()



class LedRing():
    def __init__(self, pin):
        self.NUM_LEDS = 16
        self.led_ring = NeoPixel(Pin(pin), self.NUM_LEDS)

    def set_led_color(self, index, color):
        """
        Set the color of a specific LED in the ring.
        :param index: Index of the LED (0 to NUM_LEDS-1)
        :param color: RGB color tuple (e.g., (255, 0, 0) for red)
        """
        self.led_ring[index] = color
        self.led_ring.write()

    def set_color(self, color):
        for i in range(self.NUM_LEDS):
            self.set_led_color(i, color)

    def clear_leds(self):
        """Turn off all LEDs in the ring."""
        for i in range(self.NUM_LEDS):
            self.set_led_color(i, (0, 0, 0))

    def color_cycle(self, duration=0.5):
        """
        Cycle through colors in the LED ring.
        :param duration: Time delay between each color change.
        """
        colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]  # Example colors (red, green, blue)

        for color in colors:
            for i in range(self.NUM_LEDS):
                self.set_led_color(i, color)
                time.sleep(duration)

class LedRings:
    def __init__(self, ring_pins):
        self.rings = [LedRing(pin) for pin in ring_pins]
        self.NUM_LEDS = 16

    def set_led_color(self, led_index, color):
        """
        Set the color of a specific LED in all LED rings.
        :param led_index: Index of the LED within each ring (0 to NUM_LEDS-1)
        :param color: RGB color tuple (e.g., (255, 0, 0) for red)
        """
        for ring in self.rings:
            ring.set_led_color(led_index, color)

    def set_color(self, color):
        for ring in self.rings:
            ring.set_color(color)

    def clear_leds(self):
        """Turn off all LEDs in all LED rings."""
        for ring in self.rings:
            ring.clear_leds()

    def color_cycle(self, duration=0.5):
        """
        Cycle through colors in all LED rings simultaneously.
        :param duration: Time delay between each color change.
        """
        for ring in self.rings:
            ring.color_cycle(duration)