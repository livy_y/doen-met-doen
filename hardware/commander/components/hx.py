from machine import enable_irq, disable_irq, idle
import time

class HX711:
    def __init__(self, pd_sck, dout, scale=1, gain=128):
        self.pSCK = pd_sck
        self.pOUT = dout
        self.pSCK.value(False)

        self.GAIN = 0
        self.SCALE = scale

        self.ROLLING_AVERAGE_LENGTH = 1
        self.JUMP_THRESHOLD = 30

        self.offset = 0
        self.rolling_average_list = []

        self.set_gain(gain)

    def set_gain(self, gain):
        if gain is 128:
            self.GAIN = 1
        elif gain is 64:
            self.GAIN = 3
        elif gain is 32:
            self.GAIN = 2

        self.read()

    def is_ready(self):
        return self.pOUT() == 0

    def read(self):
        # wait for the device being ready
        for _ in range(20):
            if self.pOUT() == 0:
                break
            time.sleep_ms(1)
        else:
            return None

        # shift in data, and gain & channel info
        result = 0
        for j in range(24):
            state = disable_irq()
            self.pSCK(True)
            self.pSCK(False)
            enable_irq(state)
            result = (result << 1) | self.pOUT()

        # shift back the extra bits
        result >>= self.GAIN

        # check sign
        if result > 0x7fffff:
            result -= 0x1000000

        filtered = abs(result / self.SCALE - self.offset)

        if result == 8388607:
            return None
        else:
            return filtered


    def power_down(self):
        self.pSCK.value(False)
        self.pSCK.value(True)

    def power_up(self):
        self.pSCK.value(False)
