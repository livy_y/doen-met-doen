from machine import Pin
from time import sleep, ticks_ms
from components.hx import HX711

class Jump:
    def __init__(self):
        self._dout = Pin(19, Pin.IN)
        self._pd_sck = Pin(23, Pin.OUT)
        self.hx = HX711(self._pd_sck, self._dout, scale=12)
        sleep(1)
        self.hx.offset = self.hx.read()

        self.is_jumping = False
        self.last_jump_time = 0

    @micropython.native
    def update(self):
        current_weight = self.hx.read()
        if current_weight:
            if not self.is_jumping and current_weight < 150:
                # Person has started jumping
                self.is_jumping = True
                self.last_jump_time = ticks_ms()
                return "{\"Jump\": true}"
            elif self.is_jumping and current_weight > 150:
                # Person has landed
                self.is_jumping = False
                duration = ticks_ms() - self.last_jump_time
                self.last_jump_time = 0

                return "{\"Jump\": false, \"duration\": " + str(duration) + "}"


class HX711Controller:
    def __init__(self, dout_pins, pd_sck_pins):
        self._hx711_list = []
        for dout_pin, pd_sck_pin in zip(dout_pins, pd_sck_pins):
            self._hx711_list.append(HX711(dout_pin, pd_sck_pin))

    def set_gain(self, gain):
        for hx711 in self._hx711_list:
            hx711.set_gain(gain)

    def tare_all(self, times=10):
        for hx711 in self._hx711_list:
            hx711.tare(times)

    def set_scale_all(self, scale):
        for hx711 in self._hx711_list:
            hx711.set_scale(scale)

    def get_values(self, times=10):
        return [hx711.get_units(times) for hx711 in self._hx711_list]