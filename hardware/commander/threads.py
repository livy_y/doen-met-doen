import time, network, espnow
from machine import Pin

from components.reaction import LedRings
from components.display import Display
from components.reaction import Buttons
from components.jump import Jump

LED_PIN = 12


class ComponentThread:
    def __init__(self, print_lock):
        self.print_lock = print_lock

        led = Pin(LED_PIN, Pin.OUT)
        led.value(0)

        self.led_rings = LedRings([0, 5, 16])
        self.display = Display(self.led_rings)
        self.buttons = Buttons([15, 17, 4],
                               {4: 'left_button', 17: 'middle_button', 15: 'right_button'})
        self.jump = Jump()

        led.value(1)

    @micropython.native
    def wait_and_print(self, message):
        print(f"start_commander<{message}>end")

    def loop(self):
        while True:
            buttons_message = self.buttons.get_pressed_buttons_json()
            if buttons_message:
                self.wait_and_print(buttons_message)

            jump_message = self.jump.update()
            if jump_message:
                self.wait_and_print(jump_message)


class ESPNowReceiver:
    def __init__(self, print_lock):
        self.print_lock = print_lock
        self.sta = network.WLAN(network.STA_IF)
        self.sta.active(True)
        self.sta.disconnect()
        self.esp_now = espnow.ESPNow()
        self.esp_now.active(True)

    def start_listening(self):
        print("Listening for messages ...")
        while True:
            host, msg = self.esp_now.recv()
            self.process_message(host, msg)

    def process_message(self, host, msg):
        """
        Process the received message. If the message starts with "start" and ends with "end",
        it will print the message.

        Args:
            host (str): The MAC address of the sender.
            msg (bytes): The received message.
        """
        message = msg.decode('utf-8')
        if message != "start" and message != "end":
            print(f"start_bracelet<{message}>end")
    def wait_and_print(self, message):
        while True:
            if self.print_lock.acquire():
                print(f"start_bracelet<{message}>end")
                self.print_lock.release()
                break
